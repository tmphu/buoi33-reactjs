import React, { Component } from "react";

export default class Modal extends Component {
  render() {
    return (
      <div className="row pt-5">
        <img src={this.props.content.image} alt="" className="col-4" />
        <div className="col-8">
          <p>{this.props.content.name}</p>
          <p>{this.props.content.price}</p>
          <p>{this.props.content.description}</p>
        </div>
      </div>
    );
  }
}
