import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    return (
      <div className="col-4">
        <div className="card text-left" style={{ maxWidth: "32rem" }}>
          <img className="card-img-top" src={this.props.item.image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{this.props.item.name}</h5>
            <p className="card-text">{this.props.item.price} $</p>
            <div className="d-flex justify-content-between">
              <button
                href="#"
                className="btn btn-dark"
                onClick={() => {
                  this.props.addToCart(this.props.item);
                }}
              >
                Add to cart
                <span className="pl-2">
                  <i className="fa fa-shopping-cart"></i>
                </span>
              </button>
              <button
                href="#"
                className="btn btn-primary"
                onClick={() => {
                  this.props.setStateModal(this.props.item);
                }}
              >
                View detail
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
