import React, { Component } from "react";
import ProductList from "./ProductList";
import Modal from "./Modal";
import { dataShoe } from "./dataShoe";
import Cart from "./Cart";

export default class ShoesStore extends Component {
  state = {
    list: dataShoe,
    productDetail: {},
    cart: [],
  };

  addToCart = (productItem) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === productItem.id;
    });
    if (index === -1) {
      let cartItem = { ...productItem, cartQty: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].cartQty++;
    }
    this.setState({ cart: cloneCart });
  };

  updateCart = (index, adjustment) => {
    let cloneCart = [...this.state.cart];
    if (adjustment === "INCREASE") {
      cloneCart[index].cartQty++;
    }
    if (adjustment === "DECREASE") {
      cloneCart[index].cartQty--;
      if (cloneCart[index].cartQty === 0) {
        cloneCart.splice(index, 1);
      }
    }
    this.setState({ cart: cloneCart });
  };

  removeCartItem = (index) => {
    let cloneCart = [...this.state.cart];
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };

  setStateModal = (item) => {
    this.setState({ productDetail: item });
  };
  render() {
    return (
      <>
        <h2>Shoes Shop</h2>
        <Cart cart={this.state.cart} updateCart={this.updateCart} />
        <ProductList
          productsData={this.state.list}
          addToCart={this.addToCart}
          setStateModal={this.setStateModal}
        />
        <Modal content={this.state.productDetail} />
      </>
    );
  }
}
