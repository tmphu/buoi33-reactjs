import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.cartQty}</td>
          <td>
            <button
              className="btn btn-danger mr-1"
              onClick={() => {
                this.props.updateCart(index, "DECREASE");
              }}
            >
              -
            </button>
            {item.cartQty}
            <button
              className="btn btn-success ml-1"
              onClick={() => {
                this.props.updateCart(index, "INCREASE");
              }}
            >
              +
            </button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
