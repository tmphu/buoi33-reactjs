import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  renderProductList() {
    return this.props.productsData.map((item, index) => {
      return (
        <ProductItem
          key={index}
          item={item}
          addToCart={this.props.addToCart}
          setStateModal={this.props.setStateModal}
        />
      );
    });
  }
  render() {
    return <div className="row">{this.renderProductList()}</div>;
  }
}
